# Nota importante

Este módulo fue desarrollado para [La Tienda Comprometida][1], una tienda ética
situada en Bilbao. ElenQ Technology desarrollo la primera versión para ellos y
**ellos** fueron quienes decidieron publicarlo como Software Libre. **Este
módulo existe gracias a ellos**. Puedes apoyarles comprando productos éticos en
su tienda.

Actualmente el código lo mantiene *ElenQ Technology* con la ayuda de la
comunidad. Si quieres apoyar el proyecto puedes hacerlo en [este enlace][2].

---


# Fairwire

Fairwire es un módulo de pago en FairCoin para Prestashop.

Está basadon en el módulo *bankwire-transfer* y preparado para soportar pagos
en FairCoin.


## Funcionamiento

Parecido al módulo *bankwire-transfer*, Fairwire marca los pedidos en el estado
"pago pendiente" hasta que se resuelven de forma manual.

Tiene plantillas de email específicas para enviar la información del pago al
cliente.

El módulo crea una moneda propia llamada FairCoin y un método de pago asociado
a ella. Limita la funcionalidad del método de pago a esa moneda.

La conversión de FairCoin a la moneda por defecto se realiza mediante
[fair-coin.org](https://chain.fair-coin.org/download/ticker). **Si la moneda
por defecto no aparece en esa lista debe configurarse a mano**.

> NOTA: Recuerde desactivar otros métodos de pago para la moneda FairCoin.

## Instalación

Comprima la carpeta `fairwire` folder a `zip` o `tar.gz` y suba el módulo al
apartado *Servicios y Módulos* de su panel de administración.


## Licencia

El módulo se distribuye bajo los términos de la licencia AFL3.0 (Academic Free
License).

[1]: http://latiendacomprometida.com
[2]: https://elenq.tech/en/support.html
