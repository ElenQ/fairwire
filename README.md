# Important note

This module was developed for [La Tienda Comprometida][1], an ethical shop
located in Bilbao. They asked ElenQ Technology to develop the first version of
the module and **they** decided to publish it as Free Software. **This module
exists thanks to them**. You can support them buying ethical products there.

Currently, the code is maintained in this repository by *ElenQ Technology* and
independent contributors. If you want to support the project, please consider
supporting ElenQ Technology [in this link][2].

---


# Fairwire

Fairwire is FairCoin Payment Module for Prestashop.

It was based on *bankwire-transfer* module and prepared to handle FairCoin
payments.


## Operation

Similarly to *bankwire-transfer* module, Fairwire puts the orders in a "payment
pending" state until they are cleared by hand.

It has custom email templates which send the needed information to the customer.

The module creates a custom currency called FairCoin and a payment method
associated to it and it limits its functionality to that currency.

The conversion from FairCoin to the default currency is done with
[fair-coin.org](https://chain.fair-coin.org/download/ticker). **If your default
currency is not listed in that API, you must configure it by hand**.

> NOTE: remember to deactivate other payment methods from the FairCoin
> currency.

## Installation

Compress the `fairwire` folder to `zip` or `tar.gz` and upload the module to
PrestaShop in the *Modules and Services* tab of the administrator panel.


## License

It's distributed under the terms of the AFL3.0 (Academic Free License).

[1]: http://latiendacomprometida.com
[2]: https://elenq.tech/en/support.html
