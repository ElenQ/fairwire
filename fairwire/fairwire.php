<?php
/**
 * 2018 Ekaitz Zárraga
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author     Ekaitz Zárraga <ekaitz@elenq.tech>
 *  @copyright  2018 Ekaitz Zárraga
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

if (!defined('_PS_VERSION_'))
    exit;

class FairWire extends PaymentModule
{
    protected $_html = '';
    protected $_postErrors = array();

    public $faircoin_address;
    public $extra_mail_vars;
    public function __construct()
    {
        $this->name = 'fairwire';
        $this->tab = 'payments_gateways';
        $this->version = '1.0.0';
        $this->author = 'Ekaitz Zárraga <ekaitz@elenq.tech>';
        $this->controllers = array('payment', 'validation');
        $this->is_eu_compatible = 1;

        $this->currencies = true;
        $this->currencies_mode = 'checkbox';

        $config = Configuration::getMultiple(array('FAIRCOIN_ADDRESS'));
        if (!empty($config['FAIRCOIN_ADDRESS']))
            $this->faircoin_address = $config['FAIRCOIN_ADDRESS'];

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Fairwire');
        $this->description = $this->l('Accept payments for your products via FairCoin.');
        $this->confirmUninstall = $this->l('Are you sure about removing the Fairwire module?');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => '1.6.99.99'); // TODO MIGRATE TO 1.7

        // Check if address is configured
        if (!isset($this->faircoin_address))
            $this->warning = $this->l('Faircoin address must be configured before using this module.');

        // Check if FAIRCOIN is enabled for the module!
        if (!count(Currency::checkPaymentCurrencies($this->id)))
            $this->warning = $this->l('No currency has been set for this module.');

        // TODO check if address is correctly configured


        $this->extra_mail_vars = array(
            '{faircoin_address}' => Configuration::get('FAIRCOIN_ADDRESS'),
        );
    }

    public function install()
    {
        if ( !$this->_installCurrency()
            || !parent::install()
            || !$this->registerHook('payment')
            || !$this->registerHook('displayPaymentEU')
            || !$this->registerHook('paymentReturn')
            || !$this->registerHook('updateFaircoinConversion')
            || !$this->_installOrderState()
        )
        return false;
        return true;
    }

    public function uninstall()
    {
        if (!Configuration::deleteByName('FAIRCOIN_ADDRESS') ||
            !$this->_uninstallOrderState() ||
            !$this->_uninstallCurrency() ||
            !Configuration::deleteByName('PS_OS_FAIRWIRE_PAYMENT_PENDING') ||
            !parent::uninstall())
            return false;
        return true;
    }
    protected function _installOrderState()
        // Installs a custom order state for FairWire: Payment Pending
    {
        $config_name = 'PS_OS_FAIRWIRE_PAYMENT_PENDING';
        if (Configuration::get($config_name) < 1)
        {
            $order_state = new OrderState();
            $order_state->module_name = $this->name;

            // Set mail templates
            $order_state->send_email = true;
            $order_state->template = array();
            foreach(LanguageCore::getLanguages() as $l)
                $order_state->template[$l['id_lang']] = 'fairwire';

            // Install mail templates
            foreach (LanguageCore::getLanguages() as $l)
            {
                $module_path = dirname(__FILE__).'/mails/'.$l['iso_code'].'/';
                $module_path_fallback = dirname(__FILE__).'/mails/en/';
                $application_path = dirname(__FILE__).'/../../mails/'.$l['iso_code'].'/';
                if ( !file_exists($module_path) ||
                    !file_exists($module_path.'fairwire.txt') ||
                    !file_exists($module_path.'fairwire.html') )
                    // NOTE: copy english templates if language doesn't exist
                    $module_path = $module_path_fallback;
                if (!copy($module_path.'fairwire.txt', $application_path.'fairwire.txt') ||
                    !copy($module_path.'fairwire.html', $application_path.'fairwire.html'))
                    return false;
            }

            $order_state->invoice     = false;
            $order_state->color       = '#eac448';
            $order_state->logable     = true;
            $order_state->shipped     = false;
            $order_state->unremovable = false;
            $order_state->delivery    = false;
            $order_state->hidden      = false;
            $order_state->paid        = false;
            $order_state->deleted     = false;
            $order_state->name = array((int)Configuration::get('PS_LANG_DEFAULT') => pSQL($this->l('FairWire - Payment Pending')));
            if ($order_state->add())
            {
                // Save the order State ID in Configuration database
                Configuration::updateValue($config_name, $order_state->id);

                // Copy the logo in place
                copy(dirname(__FILE__).'/views/img/status.gif', dirname(__FILE__).'/../../img/os/'.$order_state->id.'.gif');
                copy(dirname(__FILE__).'/views/img/status.gif', dirname(__FILE__).'/../../img/tmp/order_state_mini_'.$order_state->id.'.gif');
            }
            else
                return false;
        }
        return true;
    }

    protected function _uninstallOrderState()
        // Uninstalls order state for FairWire
    {
        $config_name = 'PS_OS_FAIRWIRE_PAYMENT_PENDING';
        $order_state = new OrderState(Configuration::get($config_name));

        // Delete email templates
        foreach (LanguageCore::getLanguages() as $l)
        {
            $application_path = dirname(__FILE__).'/../../mails/'.$l['iso_code'].'/';
            if (file_exists($application_path.'fairwire.txt'))
                if(!unlink($application_path.'fairwire.txt') )
                    return false;
            if (file_exists($application_path.'fairwire.html'))
                if(!unlink($application_path.'fairwire.html'))
                    return false;
        }

        return $order_state->delete();
    }

    protected function _installCurrency()
        // Installs new FairCoin currency
    {

        $currency = '';

        // Check if currency exists.
        // Currency::Exists() is not used for compatibility between versions
        // < 1.6.1.18 and 1.6.1.18
        $isocode_exists = (int) Currency::getIdByIsoCode('FAI') ;
        if($isocode_exists){
            $currency = new Currency($isocode_exists);
        }else {
            $currency = new Currency();
        }

        $currency->name             = 'FairCoin';
        $currency->iso_code         = 'FAI';
        $currency->iso_code_num     = '000';
        $currency->numeric_iso_code = '000';
        $currency->active           = true;
        $currency->sign             = 'FAIR';
        $currency->symbol           = 'FAIR';
        $currency->decimals         = 1;
        $currency->precision        = 1;
        $currency->format           = 2;
        $currency->blank            = true;

        $default_currency = new Currency ( Configuration::get('PS_CURRENCY_DEFAULT') );

        $currency->conversion_rate  = $this->get_exchange_rate($default_currency->iso_code);
        if($currency->conversion_rate == false){
            // Deactivate currency if update didn't work
            $currency->conversion_rate = 1;
            $currency->active = false;
        }
        // Set the latest time it when it was updated
        Configuration::updateValue('FAIRCOIN_LATEST_CONVERSION_RATE_UPDATE', time() );


        if($isocode_exists){
            return $currency->update();
        }else{
            return $currency->add();
        }
    }

    protected function _uninstallCurrency()
    {
        $currency = new Currency((int) Currency::getIdByIsoCode('FAI'));
        return $currency->delete();
    }

    public function get_exchange_rate ($currency_code)
        // Get official exchange rate
    {

        // API: api/ticker, api/fair-eur, api/eur-fair, etc.
        // Can use fair-eur for the automatic conversion ratio!
        // This API blocks some accesses:
        //$source_url = "http://getfaircoin.net/api/ticker";

        // This API seems to work well
        $source_url = "https://chain.fair-coin.org/download/ticker";

        $result = Tools::file_get_contents ($source_url);
        $rate_obj = json_decode(trim($result), true);
        if (!is_array($rate_obj))
            return false;

        if(!isset($rate_obj[$currency_code])){
            return false;
        }
        return 1/$rate_obj[$currency_code]['last'];
    }

    public function hookUpdateFaircoinConversion($params)
    {
        ppp('Updating conversion value');

        $default_currency = new Currency ( Configuration::get('PS_CURRENCY_DEFAULT') );
        $currency = new Currency( Currency::getIdByIsoCode('FAI') );
        $currency->conversion_rate  = $this->get_exchange_rate($default_currency->iso_code);
        if($currency->conversion_rate != false){
            // Set the latest time it when it was updated
            Configuration::updateValue('FAIRCOIN_LATEST_CONVERSION_RATE_UPDATE', time() );
            $currency->update();
        }
    }

    protected function _postValidation()
    {
        if (Tools::isSubmit('btnSubmit'))
        {
            if (!Tools::getValue('FAIRCOIN_ADDRESS'))
                $this->_postErrors[] = $this->l('FairCoin address is needed');
        }
    }

    protected function _postProcess()
    {
        if (Tools::isSubmit('btnSubmit'))
        {
            Configuration::updateValue('FAIRCOIN_ADDRESS', Tools::getValue('FAIRCOIN_ADDRESS'));
        }
        $this->_html .= $this->displayConfirmation($this->l('Settings updated'));
    }

    protected function _displayFairWire()
    {
        return $this->display(__FILE__, 'infos.tpl');
    }

    public function getContent()
    {
        if (Tools::isSubmit('btnSubmit'))
        {
            $this->_postValidation();
            if (!count($this->_postErrors))
                $this->_postProcess();
            else
                foreach ($this->_postErrors as $err)
                    $this->_html .= $this->displayError($err);
        }
        else
            $this->_html .= '<br />';

        $this->_html .= $this->_displayFairWire();
        $this->_html .= $this->renderForm();

        return $this->_html;
    }

    public function hookPayment($params)
    {
        if (!$this->active)
            return;
        if (!$this->checkCurrency($params['cart']))
            return;

        // Update FairCoin value
        // TODO in 1.7 move this to a cronjob
        $latest_time = Configuration::get('FAIRCOIN_LATEST_CONVERSION_RATE_UPDATE');
        if( time() - $latest_time > 60*60*24 ){ // only check once per day
            Hook::exec('updateFaircoinConversion');
        }

        $this->smarty->assign(array(
            'this_path' => $this->_path,
            'this_path_bw' => $this->_path,
            'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->name.'/'
        ));
        $this->context->controller->addCSS($this->_path.'views/css/fairwirepayment.css', 'all');
        return $this->display(__FILE__, 'payment.tpl');
    }

    public function hookDisplayPaymentEU($params)
    {
        if (!$this->active)
            return;

        if (!$this->checkCurrency($params['cart']))
            return;

        $payment_options = array(
            'cta_text' => $this->l('Pay using FairCoin'),
            'logo' => Media::getMediaPath(_PS_MODULE_DIR_.$this->name.'/fairwire.jpg'),
            'action' => $this->context->link->getModuleLink($this->name, 'validation', array(), true)
        );

        return $payment_options;
    }

    public function hookPaymentReturn($params)
        // Redirected here after payment
    {
        if (!$this->active)
            return;

        $state = $params['objOrder']->getCurrentState();
        if (in_array($state, array(Configuration::get('PS_OS_FAIRWIRE_PAYMENT_PENDING'), Configuration::get('PS_OS_OUTOFSTOCK'), Configuration::get('PS_OS_OUTOFSTOCK_UNPAID'))))
        {
            $this->smarty->assign(array(
                'total_to_pay' => Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false),
                'faircoin_address' => Tools::nl2br($this->faircoin_address),
                'status' => 'ok',
                'id_order' => $params['objOrder']->id
            ));
            if (isset($params['objOrder']->reference) && !empty($params['objOrder']->reference))
                $this->smarty->assign('reference', $params['objOrder']->reference);
        }
        else
            $this->smarty->assign('status', 'failed');
        return $this->display(__FILE__, 'payment_return.tpl');
    }

    public function checkCurrency($cart)
        // Check if the order currency matches the currency available in the module
    {
        // NOTE: ONLY ALLOWS FAIRCOIN
        $currency_order = new Currency($cart->id_currency);
        if( $currency_order->iso_code == 'FAI' )
            return true;
        return false;
    }

    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('FairCoin details'),
                    'icon' => 'icon-envelope'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('FairCoin Address'),
                        'name' => 'FAIRCOIN_ADDRESS',
                        'required' => true
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->id = (int)Tools::getValue('id_carrier');
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'btnSubmit';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        return array(
            'FAIRCOIN_ADDRESS' => Tools::getValue('FAIRCOIN_ADDRESS', Configuration::get('FAIRCOIN_ADDRESS')),
        );
    }
}
