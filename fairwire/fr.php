<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{fairwire}prestashop>fairwire_ad5884fbde4006fcec506c30596821a4'] = 'Fairwire';
$_MODULE['<{fairwire}prestashop>fairwire_907119576ba9f527a940e6f2121c4389'] = 'Accepte les paiements en FairCoin';
$_MODULE['<{fairwire}prestashop>fairwire_6d17da84e02c8fa3424c252596daed63'] = 'Etes-vous sûr de vouloir supprimer le module Fairwire ?';
$_MODULE['<{fairwire}prestashop>fairwire_182524eed96193ae988692e7d4104207'] = 'Vous devez configurer une adresse FairCoin avant d\'utiliser le module';
$_MODULE['<{fairwire}prestashop>fairwire_a02758d758e8bec77a33d7f392eb3f8a'] = 'Il n\'y a aucune devise configurée pour ce module.';
$_MODULE['<{fairwire}prestashop>fairwire_3e2afcb986bbad4c01239f340f34d77e'] = 'FairWire - Paiement en attente';
$_MODULE['<{fairwire}prestashop>fairwire_318c03480169b3d9216a4caf7b96a9ee'] = 'Vous devez renseigner une adresse FairCoin';
$_MODULE['<{fairwire}prestashop>fairwire_c888438d14855d7d96a2724ee9c306bd'] = 'Configuration actualisée';
$_MODULE['<{fairwire}prestashop>fairwire_bea3d664cdaa1cbdeeac203321f2cab0'] = 'Payer en FairCoin';
$_MODULE['<{fairwire}prestashop>fairwire_6c5b105552cc6af8aed9c44970c550f5'] = 'Details de FairCoin';
$_MODULE['<{fairwire}prestashop>fairwire_911b08030e8a4237e5d073fd2defa053'] = 'Adresse FairCoin';
$_MODULE['<{fairwire}prestashop>fairwire_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{fairwire}prestashop>validation_e2b7dec8fa4b498156dfee6e4c84b156'] = 'Ce moyen de paiement n\'est pas disponible';
$_MODULE['<{fairwire}prestashop>payment_execution_d56119ea3d0d1db6c8b53941c0a896b6'] = 'Paiement en FairCoin';
$_MODULE['<{fairwire}prestashop>payment_execution_f1d3b424cd68795ecaa552883759aceb'] = 'Resumé de la commande';
$_MODULE['<{fairwire}prestashop>payment_execution_879f6b8877752685a966564d072f498f'] = 'Votre panier est vide.';
$_MODULE['<{fairwire}prestashop>payment_execution_4946496dd16228e6b9fe2825082ae130'] = 'Paiement en FairCoin';
$_MODULE['<{fairwire}prestashop>payment_execution_fe0a8797cbfb327eab92bc3ba170597a'] = 'Vous avez choisi de payer en FairCoin.';
$_MODULE['<{fairwire}prestashop>payment_execution_2d77c882fcdd13b2260e6aa0f2c7e682'] = '(En savoir plus sur le FairCoin)';
$_MODULE['<{fairwire}prestashop>payment_execution_c884ed19483d45970c5bf23a681e2dd2'] = 'Resumé de votre commande:';
$_MODULE['<{fairwire}prestashop>payment_execution_e2867a925cba382f1436d1834bb52a1c'] = 'Le total à payer est: ';
$_MODULE['<{fairwire}prestashop>payment_execution_1f87346a16cf80c372065de3c54c86d9'] = '(TVA incl.)';
$_MODULE['<{fairwire}prestashop>payment_execution_35550b6e460c6c23c2d5564327144cca'] = 'Les informations de paiement s\'afficheront sur la page suivante.';
$_MODULE['<{fairwire}prestashop>payment_execution_edd87c9059d88fea45c0bd6384ce92b9'] = 'S\'il vous plait, confirmez la commande en cliquant sur \"Confirmer la commande\"';
$_MODULE['<{fairwire}prestashop>payment_execution_569fd05bdafa1712c4f6be5b153b8418'] = 'Autres moyens de paiement';
$_MODULE['<{fairwire}prestashop>payment_execution_46b9e3665f187c739c55983f757ccda0'] = 'Confirmer la commande';
$_MODULE['<{fairwire}prestashop>infos_dc279f25e5cc81b16d8f33d659fe52c6'] = 'Ce module vous permet d\'accepter les paiements sécurisés en FairCoin.';
$_MODULE['<{fairwire}prestashop>infos_99d498062669deace03d7a667243f800'] = 'Si le client sélectionne Payer en FairCoin, le statut de la commande passera à \"Paiement en attente\".';
$_MODULE['<{fairwire}prestashop>infos_8cf763feedae504992b38cee7a6b07ed'] = 'Vous devez changer le statut de la commande manuellement une fois reçue la transaction en FairCoin.';
$_MODULE['<{fairwire}prestashop>payment_308a5da73850dcdcf96efba44dc675ee'] = 'Payer en FairCoin';
$_MODULE['<{fairwire}prestashop>payment_return_88526efe38fd18179a127024aba8c1d7'] = 'Votre commande sur %s a bien été enregistrée.';
$_MODULE['<{fairwire}prestashop>payment_return_e825ede322f673ddb486ffcf4b1f7125'] = 'S\'il vous plait, réalisez à présent le virement en Faircoin:';
$_MODULE['<{fairwire}prestashop>payment_return_b2f40690858b404ed10e62bdf422c704'] = 'Montant: ';
$_MODULE['<{fairwire}prestashop>payment_return_911b08030e8a4237e5d073fd2defa053'] = 'Adresse FairCoin: ';
$_MODULE['<{fairwire}prestashop>payment_return_3d39c4f98d8dabb19ee0ed29e6e5825f'] = 'N\'oubliez pas d\'ajouter la référence de votre commande #%d dansl\'objet de la transaction.';
$_MODULE['<{fairwire}prestashop>payment_return_176942f0ca6496ece06c63b8cf75ebb8'] = 'N\'oubliez pas d\'ajouter la référence de votre commande %s dans l\'objet de la transaction.';
$_MODULE['<{fairwire}prestashop>payment_return_19c419a8a4f1cd621853376a930a2e24'] = 'Un email vous a été envoyé avec ces informations.';
$_MODULE['<{fairwire}prestashop>payment_return_b9a1cae09e5754424e33764777cfcaa0'] = 'Votre commande vous sera envoyée une fois reçu le paiement.';
$_MODULE['<{fairwire}prestashop>payment_return_ca7e41a658753c87973936d7ce2429a8'] = 'Si vous avez des questions ou des commentaires, contactez notre';
$_MODULE['<{fairwire}prestashop>payment_return_66fcf4c223bbf4c7c886d4784e1f62e4'] = 'équipe Relations client';
$_MODULE['<{fairwire}prestashop>payment_return_d15feee53d81ea16269e54d4784fa123'] = 'Nous avons détecté un problème avec votre commande. Si vous pensez que c\'est une erreur, contactez-nous.';
