{*
* 2018 Ekaitz Zárraga
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author     Ekaitz Zárraga <ekaitz@elenq.tech>
*  @copyright  2018 Ekaitz Zárraga
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}


{capture name=path}
    {l s='FairCoin payment' mod='fairwire'}
{/capture}

<h1 class="page-heading">
{l s='Order summary' mod='fairwire'}
</h1>

<h2></h2>

{assign var='current_step' value='payment'}
{include file="$tpl_dir./order-steps.tpl"}

{if $nbProducts <= 0}
    <p class="alert alert-warning">{l s='Your shopping cart is empty.' mod='fairwire'}</p>
{else}

<h3>{l s='FairCoin payment' mod='fairwire'}</h3>
<form action="{$link->getModuleLink('fairwire', 'validation', [], true)|escape:'html'}" method="post">

<div class="box cheque-box">
    <h3 class="page-subheading">
        {l s='Faircoin Payment' mod='fairwire'}
    </h3>
    <p class="cheque-indent">
        <strong class="dark">
            {l s='You have chosen to pay using FairCoin.' mod='fairwire'}
            <a href="https://fair-coin.org/">{l s='Learn more about FairCoin.' mod='fairwire'}</a><br/>
            {l s='Here is a short summary of your order:' mod='fairwire'}
        </strong>
    </p>

    <p>
        - {l s='The total amount of your order is' mod='fairwire'}

        <span id="amount" class="price">{displayPrice price=$total}</span>
        {if $use_taxes == 1}
            {l s='(tax incl.)' mod='fairwire'}
        {/if}
    </p>

    <p>
        {l s='Payment information will be displayed on the next page.' mod='fairwire'}
        <br /><br />
        <b>{l s='Please confirm your order by clicking "I confirm my order".' mod='fairwire'}</b>
    </p>
</div><!-- .cheque-box -->

<p class="cart_navigation clearfix" id="cart_navigation">

    <a class="button-exclusive btn btn-default"
       href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html':'UTF-8'}">
        <i class="icon-chevron-left"></i>{l s='Other payment methods' mod='fairwire'}
    </a>

    <button class="button btn btn-default button-medium"
            type="submit">
        <span>{l s='I confirm my order' mod='fairwire'}<i class="icon-chevron-right right"></i></span>
</button>

</form>
{/if}
